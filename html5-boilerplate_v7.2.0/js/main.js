var $target = $('main');
inView('section').on('enter', function(el) {
    var color = $(el).attr('data-background-color');
    var textcolor = $(el).attr('data-color');
    $target.css('background-color', color);
    $target.css('color', textcolor);
});

/*
jQuery(function() {
    var getWindow = jQuery(window);
    var windowHeight = getWindow.height();
    jQuery(window).scroll(function() {
        if (jQuery(window).scrollTop() > windowHeight) { jQuery("header").fadeIn(100); } else { jQuery("header").fadeOut(100); }
    });
});*/

$(window).scroll(function() {
    $("header").css({
        'opacity': 1 - (($(this).scrollTop()) / 250)

    });
    if ($(this).scrollTop() > 250) {
        $("header").css({
            'z-index': -1
        })
    } else {
        $("header").css({
            'z-index': 9999
        })
    }
});